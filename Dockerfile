FROM openjdk:8-jdk-alpine
MAINTAINER Digital Adira
ADD target/spring-boot-web-jsp-1.0.war spring-boot-web-jsp-1.0.war
ENV TZ Asia/Jakarta
RUN apk update
RUN apk add --no-cache tzdata
RUN apk add --no-cache openntpd
RUN cp /usr/share/zoneinfo/Asia/Jakarta /etc/localtime
ENTRYPOINT ["java","-jar","spring-boot-web-jsp-1.0.war"]